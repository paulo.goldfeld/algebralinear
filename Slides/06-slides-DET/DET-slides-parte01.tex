\input{../padrao-slides-branco}
\newcommand{\versao}{(V11.11.2024)}

\begin{document} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{}
\vfill
\begin{center}
\huge{Determinante}
\\\bigskip
\Large{Parte I: Motivação, Definição, Propriedades, Fórmulas}
\end{center}
\vfill
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Motivação}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Introdução}
    \begin{block}{Motivação}
        Medir ``\textbf{volume}'' (generalizado) de regiões em $\R^n$.
        \\\pause
        Significado de \textbf{volume}:
        Comprimento, em $\R$; \pause área, em $\R^2$; \pause volume, em $\R^3$; \pause hiper-volume em $\R^4$;  etc.
    \end{block}
    \pause

    \begin{block}{Roteiro}
    \begin{itemize}
        \item Começar com \textbf{paralelepípedos} (generalizados) \\
            (Segmentos, em $\R$; paralelogramos, em $\R^2$; 
            paralelepípedos, em $\R^3$; 
            hiper-paralelepípedos, em $\R^4$; etc.) 
            \pause   
        \item Identificar \textbf{propriedades} que função volume deve satisfazer.
        \pause
        \item Encontrar a \textbf{única função} com estas propriedades, o \textbf{determinante}.
        \pause
        \item Derivar mais propriedades e algoritmo eficiente para o cálculo.
    \end{itemize}
    \end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Paralelepípedos}
    \begin{definicao}[Paralelepípedo]
        O paralelepípedo gerado por
        $\vet{a}_1$, $\vet{a}_2$, \ldots, $\vet{a}_n\in\R^n$ é o conjunto
        \[
            \Big\{~ \vetv\in\R^n ~~\Big|~~ \vetv=\sum_{i=1}^n\alpha_i\vet{a}_i,
            ~\text{com } \alpha_i\in[0,1] ~\Big\}.
        \]
    \end{definicao}

    \input{desenhos/novos/DET/Paralelepipedos/paralelepipedos.pgf}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Definição Geométrica do Determinante}
    Definimos 
    a função  $\det: \R^n\times\cdots\times\R^n \to {\R} $
    por
    \[
        \det(\vet{a}_1,\ldots,\vet{a}_n) = 
        \text{volume do paralelepípedo gerado por } \vet{a}_1,\ldots,\vet{a}_n.
    \]
    \pause
    \vspace*{-1em}

    Observações:
    \begin{itemize}
        \item Na realidade $\det$  mede ``volumes \textbf{com sinal}'', similar a área com sinal da
            integral. Mas começamos com ideia mais intuitiva.
            \pause
        \item Poupamos espaço usando notação:
            \[
                \det(\vet{a}_1,\ldots,\vet{a}_n) = \Big|\vet{a}_1,\ldots,\vet{a}_n\Big|
            \]
            \pause
        \item Definimos \textbf{determinante} de matriz $A$ quadrada usando
            suas colunas $\vet{a}_j$:
            $\det A = \det(\vet{a}_1,\ldots,\vet{a}_n)$.
    \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Propriedades do Determinante}
    \begin{block}{Propriedade (i.a)}
        \[
        \det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\myred\alpha\vet{a}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
        =
        {\myred\alpha}\det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\myred\vet{a}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
        \]
        \[\scalebox{0.8}{
        \input{desenhos/novos/DET/Multlin/multlin03.pgf}
        }\]   
    \end{block}
    \pause
    Observação:
    \begin{itemize}
        \item Se $\det$ medisse volumes (sem sinal), $\alpha$ deveria sair como $|\alpha|$.
    \end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Propriedades do Determinante}
\begin{block}{Propriedade (i.b)}
\medskip
$\begin{array}{cccc}
   \det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\color[rgb]{0.50196, 0, 0}\vet{b}_k+\vet{c}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
   &
   \!\!=
   &&
   \!\!\!\!\det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\color[rgb]{0, 0.50196, 0}\vet{b}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
   \\\rule{0em}{1.3em}
   && \!\!\!\myred+ & 
   \!\!\!\!\det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\color[rgb]{0, 0, 1}\vet{c}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
   \end{array}$
\[\scalebox{0.8}{
   \input{desenhos/novos/DET/Multlin/multlin04.pgf}
}\]   
\end{block}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Propriedades do Determinante}
\begin{block}{Propriedade (i) Multi-Linearidade ou $n$-Linearidade}
\medskip
$\begin{array}{cccc}
   \det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\myred\alpha\vet{b}_k+\beta\vet{c}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
   &
   \!\!=
   &&
   \!\!\!\!{\myred\alpha}\det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\myred\vet{b}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
   \\\rule{0em}{1.3em}
   && \!\!\!\myred+ & 
   \!\!\!\!{\myred\beta}\det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\myred\vet{c}_k},\vet{a}_{k+1},\ldots\vet{a}_n)
   \end{array}$
\end{block}
\pause
\begin{exemplos}
\begin{itemize}
\item
$  ~~~
   \left|\begin{array}{rrr}{\myred1}&2&3\\{\myred-4}&5&6\\{\myred7}&-8&-9\end{array}\right|
   =
   \left|\begin{array}{rrr}{\myred1}&2&3\\{\myred0}&5&6\\{\myred0}&-8&-9\end{array}\right|
   {\myred-4}
   \left|\begin{array}{rrr}{\myred0}&2&3\\{\myred1}&5&6\\{\myred0}&-8&-9\end{array}\right|
   {\myred+7}
   \left|\begin{array}{rrr}{\myred0}&2&3\\{\myred0}&5&6\\{\myred1}&-8&-9\end{array}\right|   
$
\pause\medskip
\item
$  ~~~
               \left|\begin{array}{rr}2&6\\6&9\end{array}\right| 
   =        2  \left|\begin{array}{rr}1&6\\3&9\end{array}\right|
   = 2\left(3  \left|\begin{array}{rr}1&2\\3&3\end{array}\right|\right)
   =        6  \left|\begin{array}{rr}1&2\\3&3\end{array}\right|
$
\end{itemize}
\end{exemplos}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Propriedades do Determinante}
\begin{block}{Propriedade (ii) Alternância}
\[
   \vet{a}_i=\vet{a}_j, ~~ i\neq{j} ~~~~\Rightarrow~~~~ \det(A)=0.
\]
\[\scalebox{0.8}{
   \input{desenhos/novos/DET/Paralelepipedos/alternancia.pgf}
}\]
\end{block}
\pause\vspace*{-2em}
\begin{exemplo}
\medskip
$
   \left|\begin{array}{rrr}1&2&1\\4&-5&4\\3&6&3\end{array}\right| = 0
$   
\end{exemplo}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Propriedades do Determinante}
\begin{block}{Propriedade (iii) Hiper-Cubo Unitário}
\[
   \det(I)=\det(\vet{e}_1,\ldots,\vet{e}_n)=1.
\]
\end{block}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Definição e Propriedades}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Boa-Definição do Determinante}
\begin{teorema}[existência e unicidade da função $\det$]
   Existe uma única função $\det: \mathcal{M}_{n\times{n}}\to\R$ tal que
   \begin{itemize} 
   \item $\det$ é $n$-linear nas colunas da matriz;
   \item $\det$ é alternada e
   \item $\det(I)=1$.
   \end{itemize} 
\end{teorema}
\pause\bigskip
Veremos a ideia da prova, em breve.
\\\pause\medskip
Antes, examinaremos mais propriedades do determinante \\
(isto é, de uma função que satisfaça as 3 propriedades acima).
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Mais Propriedades do Determinante}
\begin{teorema}[trocar colunas $~~\Rightarrow~~$ trocar o sinal]
\[
     \begin{array}{r}
      \det(\vet{a}_1,\ldots,\vet{a}_{i-1},{\myred\vet{a}_i},\vet{a}_{i+1},\ldots,\vet{a}_{j-1},{\myred\vet{a}_j},\vet{a}_{j+1},\ldots,\vet{a}_n) 
     \\\rule{0em}{1.3em}
   = {\myred-}\det(\vet{a}_1,\ldots,\vet{a}_{i-1},{\myred\vet{a}_j},\vet{a}_{i+1},\ldots,\vet{a}_{j-1},{\myred\vet{a}_i},\vet{a}_{j+1},\ldots,\vet{a}_n) 
   \end{array}
\]
\end{teorema}
\pause
\textbf{Prova:}
   Para simplificar a notação, omitimos os argumentos que não variam. Assim,
   \[
            \underbrace{\det({\myred\vet{a}_i+\vet{a}_j},{\myred\vet{a}_i+\vet{a}_j})}_{=\,0} 
        =   \underbrace{\det({\myred\vet{a}_i},{\myred\vet{a}_i})}_{=\,0} 
          + \det({\myred\vet{a}_i},{\myred\vet{a}_j})
          + \det({\myred\vet{a}_j},{\myred\vet{a}_i})
          + \underbrace{\det({\myred\vet{a}_j},{\myred\vet{a}_j})}_{=\,0}   .
   \]
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Mais Propriedades do Determinante}
\begin{teorema}[colunas LD $~~\Rightarrow~~$ determinante zero]
   Se $\{\vet{a}_1,\ldots,\vet{a}_n\}$ é linearmente dependente, então $\det(A)=0$.
\end{teorema}
\pause
\textbf{Prova:}
Se o conjunto é LD, então $\displaystyle\vet{a}_k=\!\!\!\!\sum_{\scriptsize\begin{array}{c}i=1\\i\neq{k}\end{array}}^n\!\!\!\!\alpha_i\vet{a}_i$.
\pause
\vspace*{-1em}
\[
   \displaystyle\det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\myred\!\!\!\!\sum_{\scriptsize\begin{array}{c}i=1\\i\neq{k}\end{array}}^n\!\!\!\!\alpha_i\vet{a}_i},\vet{a}_{k+1},\ldots\vet{a}_n)
   =
   \displaystyle{\myred\!\!\!\!\sum_{\scriptsize\begin{array}{c}i=1\\i\neq{k}\end{array}}^n\!\!\!\!\alpha_i}\underbrace{\det(\vet{a}_1,\ldots,\vet{a}_{k-1},{\myred\vet{a}_i},\vet{a}_{k+1},\ldots\vet{a}_n)}_{=\,0}   
\]
\pause
\vspace*{-1em}
\begin{observacao}
Veremos que vale também a volta: ~~~ colunas LD $~~\Longleftrightarrow~~$ determinante zero.
\end{observacao}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Mais Propriedades do Determinante}
\begin{teorema}[determinante de matriz diagonal]
   Se $D$ é diagonal, então $\det(D)=d_{11}d_{22}\cdots d_{nn}$.
\end{teorema}
\pause\bigskip
\textbf{Prova:}
\[
   \det(D) = \matdet{cccc}{d_{11}\vet{e}_1 & d_{22}\vet{e}_2 & \cdots & d_{nn}\vet{e}_n\rule[-0.6em]{0em}{1.7em} }
           = d_{11}d_{22}\cdots d_{nn}
             \underbrace{\matdet{cccc}{\vet{e}_1 & \vet{e}_2 & \cdots & \vet{e}_n\rule[-0.6em]{0em}{1.7em} }}_{|I\,|=1}
\]
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Fórmulas}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Determinante de Matriz $2\times2$}
\begin{eqnarray*}
   \matdet{cc}{{\myred{a_{11}}}&a_{12}\\{\myred{a_{21}}}&a_{22}} 
   &=&
   \rule{2em}{0em}{\myred{a_{11}}}\rule{-2em}{0em}\underbrace{\matdet{cc}{{\myred1}&{\myteal{a_{12}}}\\{\myred0}&{\myteal{a_{22}}}}}_{{\myteal{a_{12}}}\underbrace{\matdet{cc}{1&{\myteal1}\\0&{\myteal0}}}_{0}+{\myteal{a_{22}}}\underbrace{\matdet{cc}{1&{\myteal0}\\0&{\myteal1}}}_{1}}
   +
   \rule{2em}{0em}{\myred{a_{21}}}\rule{-2em}{0em}\underbrace{\matdet{cc}{{\myred0}&{\mygreen{a_{12}}}\\{\myred1}&{\mygreen{a_{22}}}}}_{{\mygreen{a_{12}}}\underbrace{\matdet{cc}{0&{\mygreen1}\\1&{\mygreen0}}}_{-1}{\mygreen{+a_{22}}}\underbrace{\matdet{cc}{0&{\mygreen0}\\1&{\mygreen1}}}_{0}}
   \\\\\pause
   \det(A)
   &=&
   {\myred{a_{11}}}{\myteal{a_{22}}}-{\myred{a_{21}}}{\mygreen{a_{12}}}
\end{eqnarray*}

\pause\bigskip
%\[
\hspace*{4.5em}
\begin{tikzpicture}[scale=1.7]
   \path (0,0) node
     {$
    \left[
       \begin{array}{cc}
            a_{11} & a_{12}  \\
            a_{21} & a_{22}  \\
        \end{array}
    \right]
    $};
   \draw[->, thin, gray,xshift=-0.2em] (-1.5em,1em) -- (1.5em,-1em);
   \path (1.7em, -1.3em) node {$+$};
   \draw[->, thin, gray] (1.5em,1em) -- (-1.5em,-1em);
   \path (-1.7em, -1.3em) node {$-$};
\end{tikzpicture}
%\]

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Determinante de Matriz $3\times3$}
\vspace*{-1.4em}
\[\scalebox{0.74}{$
   \matdet{ccc}{a_{11}&a_{12}&a_{13}\\a_{21}&a_{22}&a_{23}\\a_{31}&a_{32}&a_{33}} 
   =
   a_{11}\matdet{ccc}{1&a_{12}&a_{13}\\0&a_{22}&a_{23}\\0&a_{32}&a_{33}} 
   +
   a_{21}\matdet{ccc}{0&a_{12}&a_{13}\\1&a_{22}&a_{23}\\0&a_{32}&a_{33}} 
   +
   a_{31}\matdet{ccc}{0&a_{12}&a_{13}\\0&a_{22}&a_{23}\\1&a_{32}&a_{33}}    
$}\]
\pause
\rule{1\textwidth}{0.5pt}
\[\scalebox{0.74}{$
   a_{11}\matdet{ccc}{1&a_{12}&a_{13}\\0&a_{22}&a_{23}\\0&a_{32}&a_{33}} 
   =
   a_{11}\left(
   a_{12}\matdet{ccc}{1&1&a_{13}\\0&0&a_{23}\\0&0&a_{33}}
   +
   %a_{11}
   a_{22}\matdet{ccc}{1&0&a_{13}\\0&1&a_{23}\\0&0&a_{33}} 
   +
   %a_{11}
   a_{32}\matdet{ccc}{1&0&a_{13}\\0&0&a_{23}\\0&1&a_{33}} 
   \right)
$}\]
\pause
\rule{1\textwidth}{0.5pt}
\[\scalebox{0.74}{$
   a_{11}a_{12}\matdet{ccc}{1&1&a_{13}\\0&0&a_{23}\\0&0&a_{33}} 
   =
   a_{11}a_{12}\resizebox{0.8em}{2.4em}{$\Big($}%\left(
   a_{13}\underbrace{\matdet{ccc}{1&1&1\\0&0&0\\0&0&0}}_{0}
   +
   %a_{11}a_{12}
   a_{23}\underbrace{\matdet{ccc}{1&1&0\\0&0&1\\0&0&0}}_{0}
   +
   %a_{11}a_{12}
   a_{33}\underbrace{\matdet{ccc}{1&1&0\\0&0&0\\0&0&1}}_{0}  
   %\right)
   \resizebox{0.8em}{2.4em}{$\Big)$}
   =
   0
$}\]
\pause\vspace*{-0.4em}
\[\scalebox{0.74}{$
   a_{11}a_{22}\matdet{ccc}{1&0&a_{13}\\0&1&a_{23}\\0&0&a_{33}} 
   =
   a_{11}a_{22}\resizebox{0.8em}{2.4em}{$\Big($}%\left(
   a_{13}\underbrace{\matdet{ccc}{1&0&1\\0&1&0\\0&0&0}}_{0}
   +
   %a_{11}a_{22}
   a_{23}\underbrace{\matdet{ccc}{1&0&0\\0&1&1\\0&0&0}}_{0}
   +
   %a_{11}a_{22}
   a_{33}\underbrace{\matdet{ccc}{1&0&0\\0&1&0\\0&0&1}}_{1}  
   %\right)
   \resizebox{0.8em}{2.4em}{$\Big)$}
   =
   a_{11}a_{22}a_{33}
$}\]
\pause\vspace*{-0.3em}
\[\scalebox{0.74}{$
   a_{11}a_{32}\matdet{ccc}{1&0&a_{13}\\0&0&a_{23}\\0&1&a_{33}} 
   =
   a_{11}a_{32}\resizebox{0.8em}{2.4em}{$\Big($}%\left(
   a_{13}\underbrace{\matdet{ccc}{1&0&1\\0&0&0\\0&1&0}}_{0}
   +
   %a_{11}a_{32}
   a_{23}\underbrace{\matdet{ccc}{1&0&0\\0&0&1\\0&1&0}}_{-1}
   +
   %a_{11}a_{32}
   a_{33}\underbrace{\matdet{ccc}{1&0&0\\0&0&0\\0&1&1}}_{0}   
   %\right) 
   \resizebox{0.8em}{2.4em}{$\Big)$}
   =
   -a_{11}a_{32}a_{23}   
$}\]

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Determinante de Matriz $3\times3$ --- continuação}
\[\scalebox{1}{$
   \matdet{ccc}{a_{11}&a_{12}&a_{13}\\a_{21}&a_{22}&a_{23}\\a_{31}&a_{32}&a_{33}} 
   =
   a_{11}\matdet{ccc}{1&a_{12}&a_{13}\\0&a_{22}&a_{23}\\0&a_{32}&a_{33}} 
   +
   a_{21}\matdet{ccc}{0&a_{12}&a_{13}\\1&a_{22}&a_{23}\\0&a_{32}&a_{33}} 
   +
   a_{31}\matdet{ccc}{0&a_{12}&a_{13}\\0&a_{22}&a_{23}\\1&a_{32}&a_{33}}    
$}\]
\rule{1\textwidth}{0.5pt}
\begin{eqnarray*}
   a_{11}\matdet{ccc}{1&a_{12}&a_{13}\\0&a_{22}&a_{23}\\0&a_{32}&a_{33}}    
   &=&
   a_{11}a_{22}a_{33} - a_{11}a_{32}a_{23}   
   \\\pause
   a_{21}\matdet{ccc}{0&a_{12}&a_{13}\\1&a_{22}&a_{23}\\0&a_{32}&a_{33}}    
   &=&
   a_{21}a_{32}a_{13} - a_{21}a_{12}a_{33} ~~\text{ e}
   \\
   a_{31}\matdet{ccc}{0&a_{12}&a_{13}\\0&a_{22}&a_{23}\\1&a_{32}&a_{33}}    
   &=&
   a_{31}a_{12}a_{23} - a_{31}a_{22}a_{13}.
\end{eqnarray*}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Determinante de Matriz $3\times3$ --- continuação}
Finalmente, 
\begin{eqnarray*}
   \matdet{ccc}{a_{11}&a_{12}&a_{13}\\a_{21}&a_{22}&a_{23}\\a_{31}&a_{32}&a_{33}} 
   &=&
   \phantom{-}
   a_{11}a_{22}a_{33}
   +
   a_{21}a_{32}a_{13} 
   +
   a_{31}a_{12}a_{23}
   \\&&
   -
   a_{11}a_{32}a_{23}
   -
   a_{21}a_{12}a_{33} 
   -
   a_{31}a_{22}a_{13}
\end{eqnarray*}

\pause\bigskip
%\[
\hspace*{4.5em}
\begin{tikzpicture}[scale=1.7]
   \path (1.1,0) node
   {$
    \left[
     \!\!
       \begin{array}{ccc}
            a_{11} & a_{12} & a_{13} \\
            a_{21} & a_{22} & a_{23} \\
            a_{31} & a_{32} & a_{33} \\
        \end{array}
    \!\!
    \right]\!\!
      \begin{array}{cc}
            a_{11} & a_{12} \\
            a_{21} & a_{22} \\
            a_{31} & a_{32} \\
      \end{array}
    $};
   \foreach \x in {0, 1.5, 3.0} {
     \begin{scope}[xshift=\x em]
      \draw[->, thin, gray] (-1.5em,1.5em) -- (4.1em,-1.5em);
      \path (4.3em, -1.8em) node {$+$};
      \draw[->, thin, gray] (4.1em,1.5em) -- (-1.5em,-1.5em);
      \path (-1.8em, -1.8em) node {$-$};
   \end{scope}
   }
\end{tikzpicture}
%\]
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Regras de Sarrus}

\hfill
$\begin{array}{c}
\begin{tikzpicture}[scale=1.7]
   \path (0,0) node
     {$
    \left[
       \begin{array}{cc}
            a_{11} & a_{12}  \\
            a_{21} & a_{22}  \\
        \end{array}
    \right]
    $};
   \draw[->, thin, gray,xshift=-0.2em] (-1.5em,1em) -- (1.5em,-1em);
   \path (1.7em, -1.3em) node {$+$};
   \draw[->, thin, gray] (1.5em,1em) -- (-1.5em,-1em);
   \path (-1.7em, -1.3em) node {$-$};
\end{tikzpicture}
\end{array}$
\hfill
$\begin{array}{c}
\begin{tikzpicture}[scale=1.7]
   \path (1.1,0) node
   {$
    \left[
     \!\!
       \begin{array}{ccc}
            a_{11} & a_{12} & a_{13} \\
            a_{21} & a_{22} & a_{23} \\
            a_{31} & a_{32} & a_{33} \\
        \end{array}
    \!\!
    \right]\!\!
      \begin{array}{cc}
            a_{11} & a_{12} \\
            a_{21} & a_{22} \\
            a_{31} & a_{32} \\
      \end{array}
    $};
   \foreach \x in {0, 1.5, 3.0} {
     \begin{scope}[xshift=\x em]
      \draw[->, thin, gray] (-1.5em,1.5em) -- (4.1em,-1.5em);
      \path (4.3em, -1.8em) node {$+$};
      \draw[->, thin, gray] (4.1em,1.5em) -- (-1.5em,-1.5em);
      \path (-1.8em, -1.8em) node {$-$};
   \end{scope}
   }
\end{tikzpicture}
\end{array}$
\hfill\rule{0em}{0em}

\bigskip
Estes diagramas mnemônicos, conhecidos como regras de Sarrus, 
\\
\textbf{não} se estendem além de matrizes $3\times3$.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Determinante de Matriz $n\times{n}$: intuição sobre fórmula}
\begin{itemize}
   \item Linearidade em cada coluna $~~\Longrightarrow~~$ somatório de termos da forma:
   \[
      {\myteal{a_{31}}}{\myred{a_{52}}}{\mygreen{a_{13}}}{\color{gray}{a_{44}}}{\myblue{a_{25}}}
      \underbrace{\matdet{ccccc}{
         0 & 0 & {\mygreen1} & 0 & 0 \\
         0 & 0 & 0 & 0 & {\myblue1} \\
         {\myteal1} & 0 & 0 & 0 & 0 \\
         0 & 0 & 0 & {\color{gray}1} & 0 \\
         0 & {\myred1} & 0 & 0 & 0 \\
      }}_{\in\;\{-1,0,1\}}
      ~~~~~~~
      \text{(Com um 1 por coluna.)}
   \] 
   \pause\vspace*{-0.4em}
   \item $n^n$ termos, mas ``só'' $n!$ não-nulos.  (Quando 1's estão em linhas distintas.)
   \pause
   \item termos não-nulos associados com permutações $\sigma\in S_n$ (todas permutações).
         \\\pause
         Exemplo, $\{1,2,3,4,5\}\stackrel{\sigma}{\mapsto}
         \{{\myteal3},{\myred5},{\mygreen1},{\color{gray}4},{\myblue2}\}$
         associada com 
         \[
         {\myteal a_{\sigma(1),1}}{\myred a_{\sigma(2),2}}{\mygreen a_{\sigma(3),3}}
         {\color{gray}a_{\sigma(4),4}}{\myblue a_{\sigma(5),5}}\sgn(\sigma)
         =\sgn(\sigma) \prod_{j=1}^n{a_{\sigma(j),j}},
         \]
   \vspace*{-1.4em}

    $\sgn(\sigma)= 1$ ou $ -1$ (sinal da permutação $\sigma$).
   %\pause\vspace*{-1.1em}
   %\item Teorema: o sinal de uma permutação está bem definido.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%$\vdots$ && $\vdots$ \\%%%%
\begin{frame}[t]{Fórmula de Leibniz para Determinante}
\begin{itemize}
    \item Fórmula de Leibniz:\quad
        \fbox{\( \displaystyle
            \det(A) = \sum_{\sigma\in{S_n}} \sgn(\sigma) \prod_{j=1}^n{a_{\sigma(j),j}}.
        \)}
        \pause
    \item Fórmula de uso \textbf{teórico}: número de termos cresce muito rapidamente!
        \\\medskip
        \qquad\qquad 
        \begin{tabular}{ccccc}
            \hline
            $n$ && $n!$ && tempo\\
            \hline
            10 && $3.6\times10^{6}$ && 3.6 micro segundos\\
            15 &&  $1.3\times10^{12}$ && 1.3 segundos\\
            20 && $2.4\times10^{18}$&& 28 dias\\
            25 && $1.5\times10^{25}$&& 492 mil anos \\
            \hline
        \end{tabular}
        \medskip

        Supondo processador i9 de 2000 \textbf{bilhões} de operações por segundo
        \pause
    \item Fórmula prova \textbf{unicidade} se  $\det$ existir, tem que ter fórmula acima.
        \\\pause
        Falta verificar \textbf{propriedades}: $n$-linear, alternada e vale 1 na identidade.
        \\\pause
        Verificação é fácil mas tediosa: será omitida.
\end{itemize}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document} 
