\input{../padrao-slides-branco}
\newcommand{\versao}{(V30.08.2023)}

\begin{document} 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{}
\vfill
\begin{center}
\huge{Sistemas Lineares}
\\\bigskip
\Large{Parte IV}
\end{center}
\vfill
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Produto Matriz-Vetor}
    \begin{definicao}[produto matriz-vetor]
        Seja
        $
            A=
            \Big[~\vet{a}_1~~\vet{a}_2~\cdots~\vet{a}_n~\Big]          
            \in\R^{m\times{n}}
        $
        e $\vet{x}=\mat{c}{x_1\\x_2\\\vdots\\x_n}\in\R^n$. Então
        \[
            A\vet{x} = \sum_{j=1}^{n}x_j\vet{a}_j.
        \]
        \pause
        {\myred
            
            O produto matriz-vetor é a combinação linear das colunas da
            matriz com coeficientes dados pelas entradas do vetor.
        }
    \end{definicao}
    
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Exemplo Numérico de Produto Matriz-Vetor}
\begin{eqnarray*}
   \mat{rrr}{1&2&3\\4&5&6}\mat{r}{7\\8\\9}
   \pause&=&
   7\mat{r}{1\\4}+8\mat{r}{2\\5}+9\mat{r}{3\\6}
   \\
   \pause&=&
   \mat{r}{7\\28}+\mat{r}{16\\40}+\mat{r}{27\\54}
   \\
   \pause&=&
   \mat{r}{7+16+27\\28+40+54}
   \\
   \pause&=&
   \mat{r}{50\\122}
\end{eqnarray*}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Segunda Interpretação para o Produto Matriz-Vetor}
\[\scalebox{0.5}{
   %\only<1>{\includegraphics{desenhos/novos/MatVec/matvec00.png}}%
   \only<1>{\includegraphics{desenhos/novos/MatVec/matvec01.png}}%
   \only<2>{\includegraphics{desenhos/novos/MatVec/matvec02.png}}%
   \only<3->{\includegraphics{desenhos/novos/MatVec/matvec03.png}}
}\]

\uncover<4->{
   {\myred A $i$-ésima entrada de $A\vet{x}$ é o produto escalar da $i$-ésima
   linha de $A$ por $\vet{x}$.}\newline
   (O produto escalar de dois vetores é
   o somatório dos produtos das entradas correspondentes.)
}

\uncover<5->{
\[
   \mat{rrr}{
      {\only<7>{\mygray}1} & {\only<7>{\mygray}2} & {\only<7>{\mygray}3} \\
      {\only<6>{\mygray}4} & {\only<6>{\mygray}5} & {\only<6>{\mygray}6} \\
   }
   \mat{r}{
      7 \\
      8 \\
      9 \\
   }
   \uncover<6-8>{
   =
   \mat{r}{
      {\only<7>{\mygray}1\times7+2\times8+3\times9} \\
      {\only<6>{\mygray}4\times7+5\times8+6\times9} \\
   }
   =
   \mat{r}{
      {\only<7>{\mygray} 50} \\ 
      {\only<6>{\mygray}122} \\
   }
   }
\]
}   
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Relação entre Sistema Linear e Produto Matriz-Vetor}

\begin{itemize}
\item $\vet{x}$ é solução de $\Big[~ A ~\Big|~ \vet{b} ~\Big] ~~\Longleftrightarrow~~ \sum_{j=1}^nx_j\vet{a}_j=\vet{b}$.
   \pause
\item Definição de produto Matriz-Vetor (CL das colunas de A): ~~~   $A\vet{x}=\sum_{j=1}^nx_j\vet{a}_j$.
   \pause
\item Segue que o sistema $\Big[~ A ~\Big|~ \vet{b} ~\Big]$ corresponde à equação vetorial $A\vet{x}=\vet{b}$.
    \pause
\end{itemize}

\bigskip
Note que \ $(A\vet{x})_i = b_i$\  é a equação\ 
\(
    a_{i1}x_1 + a_{i2}x_2 + \cdots + a_{in} x_n=b_i.
\)

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{\textbf{Linearidade} do Produto Matriz-Vetor}
\bigskip
É fácil verificar que o produto matriz vetor é \textbf{linear}:
\begin{itemize}
\item 
   $A(\vet{x}+\vet{y})=A\vet{x}+A\vet{y}, ~~ \forall\; \vet{x},\vet{y}\in\R^n $
   \quad (preserva soma vetorial)
\item
   $A(\alpha\vet{x})=\alpha{A\vet{x}}, ~~ \forall\; \alpha\in\R, ~~ \forall\; \vet{x}\in\R^n $
   \quad (preserva multiplicação por escalar)
\end{itemize}

\bigskip\pause
Assim,
\begin{itemize}
\item 
   $A\vet{0}=\vet{0}$
\item
   $A\left(\displaystyle\sum_{i=1}^p \alpha_i\vet{x}_i\right) = \displaystyle\sum_{i=1}^p \alpha_iA\vet{x}_i$
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{``Linearidade de Sistemas Lineares''}
\[
   \left.\begin{array}{l}
   \vet{x}_1~\text{solução de}~\Big[~ A ~\Big|~ \vet{b}_1 ~\Big]
   \\
   \\
   \vet{x}_2~\text{solução de}~\Big[~ A ~\Big|~ \vet{b}_2 ~\Big]
   \end{array}\right\}
   ~~\Rightarrow~~
   (\alpha\vet{x}_1+\beta\vet{x}_2)~\text{solução de}~\Big[~ A ~\Big|~ \alpha\vet{b}_1+\beta\vet{b}_2 ~\Big]
\]
\pause
pois
\[
   \left.\begin{array}{l}
   A\vet{x}_1=\vet{b}_1
   \\
   \\
   A\vet{x}_2=\vet{b}_2
   \end{array}\right\}
   ~~\Rightarrow~~
   A(\alpha\vet{x}_1+\beta\vet{x}_2) = \alpha{A}\vet{x}_1+\beta{A}\vet{x}_2 = \alpha\vet{b}_1+\beta\vet{b}_2
\]

\medskip
\pause
\begin{itemize}
\item Esta observação é particularmente relevante quando um dos sistemas é homogêneo. 
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Sistemas Homogêneos}
  \begin{definicao}[sistema homogêneo]
  \[
        \sistema{cccccc}{
            \smallskip a_{11}x_1 & +a_{12}x_2 & \cdots & +a_{1n}x_n &=&  \myred 0 \\
            \smallskip a_{21}x_1 & +a_{22}x_2 & \cdots & +a_{2n}x_n &=&  \myred 0 \\
            \smallskip   \vdots  &   \vdots   & \ddots &   \vdots   & &  \myred \vdots \\
                       a_{m1}x_1 & +a_{m2}x_2 & \cdots & +a_{mn}x_n &=&  \myred 0
        }
  \]
  \end{definicao}
  \pause

  \begin{definicao}[solução trivial]
     O vetor $\vet{0}=(0, 0, \ldots, 0)$ (zero) é sempre solução do sistema
     homogêneo. \\ Esta solução é chamada solução trivial.
  \end{definicao}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  \begin{frame}[t]{Sistemas Homogêneos}
%    \newcommand{\Vdots}{\scalebox{1}[1.7]{$\vdots$}}
%    \newcommand{\Ddots}{\scalebox{1}[1.7]{$\ddots$}}
%    Lado direito de zeros é preservado pelo escalonamento.
%    \[
%      \mat{ccc|c}{
%        \star  & \cdots & \star  &    0   \\
%        \Vdots & \Ddots & \Vdots & \Vdots \\
%        \star  & \cdots & \star  &    0   \rule{0em}{1.3em}
%      }
%      \;\;\;\sim\;\;\;
%      \mat{ccc|c}{
%            *   & \cdots &    *   &    0   \\
%         \vdots & \ddots & \vdots & \vdots \\
%            *   & \cdots &    *   &    0
%      }
%    \]
%    \let\Vdots\undefined
%    \let\Ddots\undefined
%  
%    \pause
%    \bigskip
%    Forma escalonada nunca apresenta linha
%    $\mat{ccc|c}{0&\cdots&0&\mysquare}$.
%  
%    \pause
%    \bigskip
%    Determina-se $p$ (escalonamento):
%  
%    \begin{itemize}
%    \item
%    %\bigskip
%    $p=n\;\;\;\Rightarrow\;\;\;$solução única (apenas a trivial)
%  
%    \item
%    %\bigskip
%    $p<n\;\;\;\Rightarrow\;\;\;$infinitas soluções, $(n-p)$
%    variáveis livres  
%    \end{itemize}
%  \end{frame}
%  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Sistemas Não-Homogêneo e Homogêneo Associado}
\[
   \left.\begin{array}{l}
   A\vet{x}_p=\vet{b}
   \\
   \\
   A\vet{x}_h=\vet{0}
   \end{array}\right\}
   ~~\Rightarrow~~
   A(\vet{x}_p+\vet{x}_h) = \vet{b}
\]
A soma de uma solução particular com uma do sistema homogêneo\\ é também uma solução particular.

\bigskip
\pause
\[
   \left.\begin{array}{l}
   A\vet{x}_{p}=\vet{b}
   \\
   \\
   A\vet{x}_{q}=\vet{b}
   \end{array}\right\}
   ~~\Rightarrow~~
   A(\vet{x}_{q}-\vet{x}_{p}) = \vet{0}
\]
A diferença entre duas soluções particulares é solução do sistema homogêneo.

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Sistemas Não-Homogêneo e Homogêneo Associado}

\hspace*{-1em}
\scalebox{0.95}{$
  \mat{rrrr|r}{
                 0  &              3  &              9  &              3  &               -9  \\
                 0  &              1  &              3  &              2  &                1  \\    
                 0  &              2  &              6  &              3  &               -2  \\
  }
  \only<2>{
     \sim
     \mat{rrrr|r}{
                 0  &              1  &              3  &              0  &               -7  \\
                 0  &              0  &              0  &              1  &                4  \\    
     }
  }
  \uncover<3->{
  \uncover<3>{\sim}
  \mat{rrrr|r}{
        \uncover<4->{1} & \uncover<4->{0} & \uncover<4->{0} & \uncover<4->{0} &  \uncover<4->{ r} \\
                     0  &              1  &              3  &              0  &               -7  \\
        \uncover<4->{0} & \uncover<4->{0} & \uncover<4->{1} & \uncover<4->{0} &  \uncover<4->{ s} \\
                     0  &              0  &              0  &              1  &                4  \\    
     }
  }
  \uncover<5->{
    \sim
    \only<1-5>{
       \mat{rrrr|rrrrrrr}{
          1 & 0 & 0 & 0 &&    && 1 & r &   &   \\
          0 & 1 & 0 & 0 && -7 &&   &   &-3 & s \\
          0 & 0 & 1 & 0 &&    &&   &   & 1 & s \\
          0 & 0 & 0 & 1 &&  4 &&   &   &   &   \\
       }
    }
    \only<6->{
       \mat{rrrr|r|r|r|r|r|r|r}{
          \cline{6-6}\cline{8-8}\cline{10-10}
          1 & 0 & 0 & 0 &&    && 1 & r &   &   \\
          0 & 1 & 0 & 0 && -7 &&   &   &-3 & s \\
          0 & 0 & 1 & 0 &&    &&   &   & 1 & s \\
          0 & 0 & 0 & 1 &&  4 &&   &   &   &   \\
          \cline{6-6}\cline{8-8}\cline{10-10}
       }
    }
  }
$}  

\uncover<6->{
  \medskip Conjunto-solução: \quad
  $\Big\{~(0,-7,0,4)+r(1,0,0,0)+s(0,-3,1,0)\;~\Big|~\; r,s\in\R~\Big\}$
}

%----------------------------
\medskip
\hspace*{-1em}
\scalebox{0.95}{$
  \mat{rrrr|r}{
                 0  &              3  &              9  &              3  &                0  \\
                 0  &              1  &              3  &              2  &                0  \\    
                 0  &              2  &              6  &              3  &     \phantom{-}0  \\
  }
  \only<2>{
     \sim
     \mat{rrrr|r}{
                 0  &              1  &              3  &              0  &                0  \\
                 0  &              0  &              0  &              1  &     \phantom{-}0  \\    
     }
  }
  \uncover<3->{
  \uncover<3>{\sim}
  \mat{rrrr|r}{
        \uncover<4->{1} & \uncover<4->{0} & \uncover<4->{0} & \uncover<4->{0} &  \uncover<4->{ r} \\
                     0  &              1  &              3  &              0  &                0  \\
        \uncover<4->{0} & \uncover<4->{0} & \uncover<4->{1} & \uncover<4->{0} &  \uncover<4->{ s} \\
                     0  &              0  &              0  &              1  &     \phantom{-}0  \\    
     }
  }
  \uncover<5->{
    \sim
    \only<1-5>{
       \mat{rrrr|rrrrrrr}{
          1 & 0 & 0 & 0 &&              && 1 & r &   &   \\
          0 & 1 & 0 & 0 && \phantom{-7} &&   &   &-3 & s \\
          0 & 0 & 1 & 0 &&              &&   &   & 1 & s \\
          0 & 0 & 0 & 1 &&              &&   &   &   &   \\
       }
    }
    \only<6->{
       \mat{rrrr|r|r|r|r|r|r|r}{
          \cline{6-6}\cline{8-8}\cline{10-10}
          1 & 0 & 0 & 0 &&              && 1 & r &   &   \\
          0 & 1 & 0 & 0 && \phantom{-7} &&   &   &-3 & s \\
          0 & 0 & 1 & 0 &&              &&   &   & 1 & s \\
          0 & 0 & 0 & 1 &&              &&   &   &   &   \\
          \cline{6-6}\cline{8-8}\cline{10-10}
       }
    }
  }
$}  

\medskip
\uncover<6->{
  Conjunto-solução: \quad
  $\Big\{~\phantom{(0,-7,0,4)+}r(1,0,0,0)+s(0,-3,1,0)\;~\Big|~\; r,s\in\R~\Big\}$
}

\bigskip
\vfill

\uncover<7->{
    \textbf{Conclusão:}
    Se um SL não-homogêneo admite solução, então seu conjunto-solução é uma
    translação daquele do SL homogêneo associado.
}
\end{frame}


\end{document}













%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[t]{Sistemas com Mesma Matriz de Coeficientes}
   %\vspace*{-1.0\baselineskip}
   \[
      \begin{array}{ccccc}
         \!\!\mat{rr|r}{
            1 & 2 & 4  \\
            2 & 5 & \phantom{-}9
         }\pause
         &\!\!\!\!\sim&
         \!\!\!\!\mat{rr|r}{
            1 & 2 & 4  \\
            0 & 1 & \phantom{-}1
         }\pause
         &\!\!\!\!\sim&
         \!\!\!\!\mat{rr|r}{
            1 & 0 &  2  \\
            0 & 1 &  \phantom{-}1
         }\pause
         \\\\\hline\\
         \!\!\mat{rr|r}{
            1 & 2 & -1 \\
            2 & 5 & -3
         }\pause
         &\!\!\!\!\sim&
         \!\!\!\!\mat{rr|r}{
            1 & 2 & -1 \\
            0 & 1 & -1
         }\pause
         &\!\!\!\!\sim&
         \!\!\!\!\mat{rr|r}{
            1 & 0 &   1 \\
            0 & 1 &  -1
         }\pause
         \\\\\hline\\
         \!\!\mat{rr|rr}{
            1 & 2 & 4 & -1 \\
            2 & 5 & 9 & -3
         }\pause
         &\!\!\!\!\sim&
         \!\!\!\!\mat{rr|rr}{
            1 & 2 & 4 & -1 \\
            0 & 1 & 1 & -1
         }\pause
         &\!\!\!\!\sim&
         \!\!\!\!\mat{rr|rr}{
            1 & 0 &  2 &  1 \\
            0 & 1 &  1 & -1
         }
      \end{array}
   \]

   \pause
   \bigskip
   Evitamos ``retrabalho'' aumentando a matriz
   com vários lados direitos de uma vez.
\end{frame}

