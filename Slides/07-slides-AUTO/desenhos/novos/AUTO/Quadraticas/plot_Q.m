function [] = plot_Q(A)
  figure
  A = (A+A')/2;
  X = linspace(-2,2,31);
  Y = X;
  [XX,YY] = meshgrid(X,Y);
  ZZ = A(1,1)*XX.^2 + A(2,2)*YY.^2 + A(1,2)*XX.*YY;
  surface(XX,YY,ZZ);
  hold on
  plot3(XX(:,1),YY(:,1),ZZ(:,1),'k','LineWidth',3)
  plot3(XX(:,end),YY(:,end),ZZ(:,end),'k','LineWidth',3)
  plot3(XX(1,:),YY(1,:),ZZ(1,:),'k','LineWidth',3)
  plot3(XX(end,:),YY(end,:),ZZ(end,:),'k','LineWidth',3)
##  plot3(XX(:,16),YY(:,16),ZZ(:,16),'w','LineWidth',5)
##  plot3(XX(16,:),YY(16,:),ZZ(16,:),'w','LineWidth',5)
  colormap jet
##  shading interp
  axis off
##  figure
##  contour(XX,YY,ZZ,'LineWidth',3)
##  colormap jet
##  axis off
endfunction
