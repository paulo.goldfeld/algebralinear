set table "rascunho-PGF/PI-erf2.table"; set format "%.5f"
set samples 100.0; plot [x=0:1] exp(-x) - (2*sin(3.1415*x)*0.395 + 2*sin(2*3.1415*x)*0.0981 + 2*sin(3*3.1415*x)*0.1435 + 2*sin(4*3.1415*x)*0.0499 )
